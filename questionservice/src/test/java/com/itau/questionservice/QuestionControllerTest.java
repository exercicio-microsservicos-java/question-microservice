package com.itau.questionservice;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.questionservice.controllers.QuestionController;
import com.itau.questionservice.models.Question;
import com.itau.questionservice.repositories.QuestionRepository;


@RunWith(SpringRunner.class)
@WebMvcTest(QuestionController.class)
public class QuestionControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	QuestionRepository questionRepository;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void testargetQuestions() throws Exception {

		ArrayList<Question> resposta = new ArrayList<Question>();
		
		Question question1 = new Question();
		question1.setAnswer(4);
		question1.setCategory("Beleza");
		question1.setId(1);
		question1.setOption1("David Backham");
		question1.setOption2("Cristiano Ronaldo");
		question1.setOption3("Ronaldinho Gaucho");
		question1.setOption4("Rharamys");
		question1.setTitle("Quem é o cara mais gato?");
		resposta.add(question1);
		
		Question question2 = new Question();
		question2.setAnswer(1);
		question2.setCategory("categoria");
		question2.setId(2);
		question2.setOption1("opcao1");
		question2.setOption2("opcao2");
		question2.setOption3("opcao3");
		question2.setOption4("opcao4");
		question2.setTitle("Pergunta?");		
 		resposta.add(question2);
		
		when(questionRepository.findAll()).thenReturn(resposta);		
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/questions").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		String expected = "["
				+ "{\"answer\":4,"
				+ "\"category\":\"Beleza\","
				+ "\"id\":1,"
				+ "\"option1\":\"David Backham\","
				+ "\"option2\":\"Cristiano Ronaldo\","
				+ "\"option3\":\"Ronaldinho Gaucho\","
				+ "\"option4\":\"Rharamys\","
				+ "\"title\":\"Quem é o cara mais gato?\""
				+ "}"
				+ ","
				+ "{\"answer\":1,"
				+ "\"category\":\"categoria\","
				+ "\"id\":2,"
				+ "\"option1\":\"opcao1\","
				+ "\"option2\":\"opcao2\","
				+ "\"option3\":\"opcao3\","
				+ "\"option4\":\"opcao4\","
				+ "\"title\":\"Pergunta?\""
				+ "}"
				+ "]";

		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}
}

