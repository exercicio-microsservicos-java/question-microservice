package com.itau.questionservice.models;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import logs.Trackable;

public class QuestionsResponse implements Trackable {

	private List<Question> questionsResponse;

	public QuestionsResponse (List<Question> questionsResponse) {
		this.questionsResponse = questionsResponse;
	}
	
	@Override
	public String getTrackedType() {
		return QuestionsResponse.class.getSimpleName();
	}

	@Override
	public Map<String, String> getTrackedProperties() {
		
		for (int i = 0; i< this.questionsResponse.size(); i++) {
			HashMap<String,String> questionResponse = new HashMap<String,String>();
			questionResponse.put("title", questionsResponse.get(i).getTitle());
			questionResponse.put("category", questionsResponse.get(i).getCategory());
			questionResponse.put("option1", questionsResponse.get(i).getOption1());
			questionResponse.put("option2", questionsResponse.get(i).getOption2());
			questionResponse.put("option3", questionsResponse.get(i).getOption3());
			questionResponse.put("option4", questionsResponse.get(i).getOption4());
			questionResponse.put("answer", String.valueOf(questionsResponse.get(i).getAnswer()));
				
		}
		return null;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i =0 ; i< questionsResponse.size(); i++) {
			sb.append(questionsResponse.get(i).toString());
			if (i != questionsResponse.size()-1) {
				sb.append(", ");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
	
}
