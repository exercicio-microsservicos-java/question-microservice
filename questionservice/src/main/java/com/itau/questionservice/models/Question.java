package com.itau.questionservice.models;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import logs.Trackable;

@Entity
public class Question implements Trackable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Size(min=2)
	@NotNull
	private String title;
	
	@NotNull
	private String category;
	
	@NotNull
	private String option1;

	@NotNull
	private String option2;

	@NotNull
	private String option3;

	@NotNull
	private String option4;

	@NotNull
	private int answer;
	
	public Question (int id, String title, String category, String option1, String option2, String option3, String option4, int answer) {
		this.id = id;
		this.title = title;
		this.category = category;
		this.option1 = option1;
		this.option2 = option2;
		this.option3 = option3;
		this.option4 = option4;
		this.answer = answer;
	}

	public Question() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getOption1() {
		return option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public String getOption2() {
		return option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	public String getOption3() {
		return option3;
	}

	public void setOption3(String option3) {
		this.option3 = option3;
	}

	public String getOption4() {
		return option4;
	}

	public void setOption4(String option4) {
		this.option4 = option4;
	}

	public int getAnswer() {
		return answer;
	}

	public void setAnswer(int answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("title=" + this.title);
		sb.append(", category=" + this.category);
		sb.append(", option1=" + this.option1);
		sb.append(", option2=" + this.option2);
		sb.append(", option3=" + this.option3);
		sb.append(", option4=" + this.option4);
		sb.append(", answer=" + this.answer);
		
		return sb.toString();
	}

	@Override
	public String getTrackedType() {
		return Question.class.getSimpleName();
	}

	@Override
	public Map<String, String> getTrackedProperties() {
		Map<String,String> map = new HashMap<String,String>();
		map.put("title", this.title);
		map.put("category", this.category);
		map.put("option1", this.option1);
		map.put("option2", this.option2);
		map.put("option3", this.option3);
		map.put("option4", this.option4);
		map.put("answer", String.valueOf(this.answer));
		return map;
	}
}
