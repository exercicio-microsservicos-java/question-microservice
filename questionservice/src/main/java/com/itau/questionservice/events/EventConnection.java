package com.itau.questionservice.events;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itau.questionservice.repositories.QuestionRepository;

@Component
public class EventConnection {

	private static String QUEUE_NAME1 = "f.queue.questions.full";
	private static String QUEUE_NAME2 = "f.queue.questions.id";
	private static String AMQ_SERVER = "tcp://amq.oramosmarcos.com:61616";

	@Autowired
	QuestionRepository questionRepository;

	@Autowired
	public void configuration() {
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(AMQ_SERVER);
			Connection connection = connectionFactory.createConnection();
			connection.start();
			
	    	// Criar uma sessão
	    	Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	    	
	    	// A fila que queremos popular
	    	Queue queue1 = session.createQueue(QUEUE_NAME1);
	    	Queue queue2 = session.createQueue(QUEUE_NAME2);
	    	
	    	MessageConsumer consumer1 = session.createConsumer(queue1);
	    	MessageConsumer consumer2 = session.createConsumer(queue2);
	    	consumer1.setMessageListener(new QuestionListener(session,"full",questionRepository));
	    	consumer2.setMessageListener(new QuestionListener(session,"id",questionRepository));
		} catch (JMSException ex) {
			ex.printStackTrace();
		}
	}
}
