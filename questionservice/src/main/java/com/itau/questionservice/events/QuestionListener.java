package com.itau.questionservice.events;

import java.util.ArrayList;
import java.util.HashMap;

import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import com.itau.questionservice.models.Question;
import com.itau.questionservice.repositories.QuestionRepository;

import logs.Events;
import logs.LogProducer;

public class QuestionListener implements MessageListener{
	
	QuestionRepository questionRepository;
	
	Session session;
	
	String sessionType;

	public QuestionListener(Session session, String sessionType, QuestionRepository questionRepository) {
		this.session = session;
		this.sessionType = sessionType;
		this.questionRepository = questionRepository;
	}

	@Override
	public void onMessage(Message request) {
		MapMessage requestMap = (MapMessage) request;
		try {
			int number = Integer.parseInt(requestMap.getString("quantity"));
			ArrayList<Question> questions = questionRepository.findAll();
			int index;
			
			MapMessage response = session.createMapMessage();
			response.setJMSCorrelationID(request.getJMSCorrelationID());
			MessageProducer producer = session.createProducer(request.getJMSReplyTo());
			
			if (this.sessionType.equals("full")) {
				ArrayList<HashMap<String,String>> questionsResponse = new ArrayList<HashMap<String,String>>();
				while(questions.size() > 0 && number > 0) {
					index = (int)Math.ceil(Math.random()*questions.size() -1);
					Question question = questions.get(index);
					
					HashMap<String,String> questionResponse = new HashMap<String,String>();
					
					questionResponse.put("title", question.getTitle());
					questionResponse.put("category", question.getCategory());
					questionResponse.put("option1", question.getOption1());
					questionResponse.put("option2", question.getOption2());
					questionResponse.put("option3", question.getOption3());
					questionResponse.put("option4", question.getOption4());
					questionResponse.put("answer", String.valueOf(question.getAnswer()));
					
					questionsResponse.add(questionResponse);
					questions.remove(index);
					number--;	
				}
				if (number != 0) {
					questionsResponse.clear();
				}
				
				StringBuilder sb = new StringBuilder();
				sb.append("[");
				for (int i =0 ; i< questionsResponse.size(); i++) {
					sb.append(questionsResponse.get(i).toString());
					if (i != questionsResponse.size()-1) {
						sb.append(", ");
					}
				}
				sb.append("]");
				
				String log = sb.toString();
				
				LogProducer.createProducerThread(log);
//				Events.emmit("questions", "getQuestions", log);
				
				response.setObject("questions", questionsResponse);
			} else {
				ArrayList<Integer> indexes = new ArrayList<Integer>();
				while(questions.size() > 0 && number > 0) {
					index = (int)Math.ceil(Math.random()*questions.size() -1);
					indexes.add(index);
					questions.remove(index);
					number--;	
				}
				if (number != 0) {
					indexes.clear();
				}
				response.setObject("questions", indexes);	
			}
			
			producer.send(response);
			producer.close();
		
		} catch (Exception e) {
			System.out.println("Deu ruim!");
		}
	}
	
}
