package com.itau.questionservice.repositories;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import com.itau.questionservice.models.Question;

public interface QuestionRepository extends CrudRepository<Question, Integer>{
	
	public ArrayList<Question> findAll();
}
