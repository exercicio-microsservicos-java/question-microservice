package com.itau.questionservice.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.questionservice.models.Question;
import com.itau.questionservice.models.QuestionResponse;
import com.itau.questionservice.repositories.QuestionRepository;

import logs.LogProducer;

@RestController
public class QuestionController {
	
	@Autowired
	QuestionRepository questionRepository;

	@RequestMapping(method=RequestMethod.GET, path="/questions")
	public Iterable<Question> getQuestions() {
		return questionRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/question/{id}")
	public ResponseEntity<Question> getQuestion(@PathVariable int id) {
		Optional<Question> optionalQuestion = questionRepository.findById(id);
		if (!optionalQuestion.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(optionalQuestion.get());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/questions/{number}")
	public ResponseEntity<List<QuestionResponse>> getQuestionList(@PathVariable int number) {
		ArrayList<Question> questions = questionRepository.findAll();
		List<QuestionResponse> questionsResponse = new ArrayList<QuestionResponse>();
		int index;
		while(questions.size() > 0 && number > 0) {
			index = (int)Math.ceil(Math.random()*questions.size() -1);
			Question question = questions.get(index);
			QuestionResponse response = new QuestionResponse();
			response.setIndiceQuestion(question.getId());
			response.setOption1(question.getOption1());
			response.setOption2(question.getOption2());
			response.setOption3(question.getOption3());
			response.setOption4(question.getOption4());
			response.setTitle(question.getTitle());
			response.setAnswer(question.getAnswer());
			questionsResponse.add(response);
			questions.remove(index);
			number--;
		}
		if (number != 0) {
			return ResponseEntity.status(416).build();
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i =0 ; i< questionsResponse.size(); i++) {
			sb.append("{" + questionsResponse.get(i).toString() + "}");
			if (i != questionsResponse.size()-1) {
				sb.append(", ");
			}
		}
		sb.append("]");
		
		String log = sb.toString();
		LogProducer.createProducerThread(log);
		
		return ResponseEntity.ok(questionsResponse);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/questions")
	public ResponseEntity<Iterable<Question>> loadQuestions(@RequestBody Iterable<Question> questoes) {
		try {
			questionRepository.saveAll(questoes);
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(questoes);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/question")
	public ResponseEntity<Question> loadQuestion(@RequestBody Question questao) {
		try {
			questionRepository.save(questao);
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(questao);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/question/{id}")
	public ResponseEntity<Question> deleteQuestion(@PathVariable int id) {
		Optional<Question> questao = questionRepository.findById(id);
		if (!questao.isPresent()) {
			ResponseEntity.notFound().build();
		}
		questionRepository.delete(questao.get());
		return ResponseEntity.ok(questao.get());
	}
	
}
