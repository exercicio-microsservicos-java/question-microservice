package logs;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;


public class LogProducer {
	
	private static final String TOPIC = "questions";
	private static final String SERVERS = "10.162.107.229:9090;10.162.107.229:9091;10.162.107.229:9092";

	public static KafkaProducer<String, String> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "f.produtor.questions");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		KafkaProducer<String, String> producer = new KafkaProducer<>(props);
		return producer;
	}

	public static void createProducerThread(String log) {

		Random random = new Random();
		KafkaProducer<String, String> producer = createProducer();

		final String topico = TOPIC;
		final String key = "f.log." + random.nextInt(10000000);
		final String body = MessageFormat.format("[{0}]-{1} {2}", LocalDateTime.now().toString(), "Questions requisitadas:", log);

		ProducerRecord<String, String> record = new ProducerRecord<>(topico, key, body);

		try {
			producer.send(record).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		producer.flush();
		producer.close();
	}

}
