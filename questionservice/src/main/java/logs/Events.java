package logs;

public class Events {
	
	public static void emmit(String source, String action) {
		emmit(source, action, null);
	}
	
	public static void emmit(String source, String action, Trackable trackable) {
		Event event = new Event(source, action, trackable);
		LogEngine logger = LogEngineFactory.getEngine(LogEngineFactory.MYSQL);
		logger.log(event);
	}
}
