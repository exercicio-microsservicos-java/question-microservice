package logs;

public abstract class LogEngine {

	public void log(Event message) {
		log(message.toString());
	}
	
	public abstract void log(String message);
}
