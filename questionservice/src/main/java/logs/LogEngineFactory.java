package logs;

public abstract class LogEngineFactory {

	public static final int SIMPLE = 0;
	public static final int MYSQL = 1;
	

	public static LogEngine getEngine(int type) {
		switch (type) {
		case SIMPLE:
			return new SimpleLogEngine();
		case MYSQL:
			return new RepositoryLogEngine();
		default:
			return null;
		}
	}
}
