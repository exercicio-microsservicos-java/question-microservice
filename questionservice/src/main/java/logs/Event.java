package logs;

import java.util.Map;
import java.util.Set;

public class Event {

	String source;
	
	String action;
	
	Trackable trackable;

	public Event(String source, String action) {
		super();
		this.source = source;
		this.action = action;
		this.trackable = null;
	}
	
	public Event(String source, String action, Trackable trackable) {
		super();
		this.source = source;
		this.action = action;
		this.trackable = trackable;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Trackable getTrackable() {
		return trackable;
	}

	public void setTrackable(Trackable trackable) {
		this.trackable = trackable;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(source).append(":").append(action);
		
		if(trackable != null) {
			Map<String, String> props = trackable.getTrackedProperties();
			Set<String> keys = props.keySet();
			
			buffer.append(":");
			buffer.append(trackable.getTrackedType());
			buffer.append("{");
			for(String key:keys) {
				buffer.append(key).append("=").append(props.get(key)).append(";");
			}
			buffer.append("}");				
		}
		return buffer.toString();
	}
}
