package logs;

import java.util.Map;

public interface Trackable {
	
	public String getTrackedType();
	public Map<String, String> getTrackedProperties();

}
